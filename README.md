# Filter lists by Salim

Custom filter lists for [uBlock Origin](https://github.com/gorhill/uBlock) and compatible software, curated by [me](https://gitlab.com/salim-b).

## Salim's Blocklist

[`salims_blocklist.txt`](salims_blocklist.txt) contains filter rules that mainly target elements that are not blocked by any of the stock filter lists included in uBlock Origin[^non-default], but which Salim wants to be blocked/hidden. Typically, these filter rules don't meet the acceptance criteria of any filter list just mentioned.

To subscribe to _Salim's Blocklist_, click [here](abp:subscribe?location=https://gitlab.com/salim_b/filter_lists/raw/master/salims_blocklist.txt&title=Salim%27s%20Blocklist) or manually add the following URL to your content blocker:

```url
https://gitlab.com/salim_b/filter_lists/raw/master/salims_blocklist.txt
```


[^non-default]: Note that not all of the [stock filter lists](https://github.com/gorhill/uBlock/wiki/Dashboard:-Filter-lists#stock-filter-lists) are enabled by default.


## Salim's Allowlist

[`salims_allowlist.txt`](salims_allowlist.txt) contains exception filter rules that mainly target elements that are blocked/hidden by [Fanboy's EasyList add-ons](https://github.com/easylist/easylist/tree/master/fanboy-addon), but which Salim wants to be able to see/use.

To subscribe to _Salim's Allowlist_, click [here](abp:subscribe?location=https://gitlab.com/salim_b/filter_lists/raw/master/salims_allowlist.txt&title=Salim%27s%20Allowlist) or manually add the following URL to your content blocker:

```url
https://gitlab.com/salim_b/filter_lists/raw/master/salims_allowlist.txt
```
